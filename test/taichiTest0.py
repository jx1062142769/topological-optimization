import taichi as ti
import numpy as np

ti.init(arch=ti.gpu)
nelx = 6
nely = 4
volfrac = 0.5
x = ti.field(dtype=ti.f32, shape=(nely, nelx))
dc = ti.field(ti.f32, shape=(nely, nelx))

dc_np = np.array([
    [-22.9263, -22.8929, -22.7614, -22.4480, -22.0604, -21.7551],
    [-22.8508, -22.8178, -22.8013, -22.5493, -22.1742, -21.8602],
    [-22.8079, -22.6918, -22.6294, -22.4018, -22.0297, -21.7356],
    [-22.9233, -22.8505, -22.6319, -22.2843, -21.8578, -21.6024]
])
x_np_in = np.array([
    [0.8502,    0.7311,    0.6062,    0.4475,    0.2583,    0.0893],
    [0.2751,    0.3441,    0.4852,    0.5605,    0.5098,    0.3196],
    [0.2517,    0.3101,    0.3950,    0.4536,    0.5318,    0.5805],
    [0.7837,    0.7120,    0.6388,    0.5704,    0.5649,    0.7309]
])
x_np_out = np.array([
    [0.8589,    0.7381,    0.6102,    0.4474,    0.2560,    0.0879],
    [0.2774,    0.3468,    0.4888,    0.5615,    0.5065,    0.3153],
    [0.2536,    0.3117,    0.3965,    0.4530,    0.5266,    0.5710],
    [0.7917,    0.7181,    0.6412,    0.5681,    0.5572,    0.7168]
])


@ti.kernel
def calc(frac: ti.f32):
    for i, j in x:
        x[i, j] = 1 * frac


def oc():
    l1 = 0
    l2 = 100000
    move = 0.2
    x_oc = x.to_numpy()
    x_new = x_oc
    dc_oc = dc.to_numpy()
    while l2 - l1 > 1e-4:
        lmid = 0.5 * (l2 + l1)
        # print("x_oc: ")
        # print(x_oc)
        x_new = np.maximum(0.001, np.maximum(x_oc - move, np.minimum(1., np.minimum(x_oc + move, x_oc * np.sqrt(-dc_oc / lmid)))))
        print("x_new: ")
        print(x_new)
        if np.sum(x_new) - volfrac * nelx * nely > 0:
            l1 = lmid
        else:
            l2 = lmid
    x.from_numpy(x_new)


dc.from_numpy(dc_np)
x.from_numpy(x_np_in)
oc()
print(x.to_numpy())

gui = ti.GUI("99 line topo", res=(nely, nelx))
while gui.running:
    gui.set_image(x)
    gui.show()
