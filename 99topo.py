import taichi as ti
import numpy as np
import math
import time
from scipy.sparse import csr_matrix
from scipy.sparse import csc_matrix
from scipy.sparse.linalg import spsolve

ti.init(arch=ti.cpu)

# Display
gui_x = 600
gui_y = 400
display = ti.field(ti.f64, shape=(gui_x, gui_y)) # field for display

nelx = 60
nely = 40
volfrac = 0.5
penal = 3.0
rmin = 1.5
rminf = math.floor(rmin)
# initialize
x = ti.field(dtype=ti.f64, shape=(nely, nelx))
dc = ti.field(ti.f64, shape=(nely, nelx))  # derivative of compliance
dc_np = ti.field(ti.f64, shape=(nely, nelx))  # 存优化之前的
compliance = ti.field(ti.f64, shape=()) # compliance

KE = ti.field(dtype=ti.f64, shape=(8, 8))
K = ti.field(ti.f64, shape=(2 * (nelx + 1) * (nely + 1), 2 * (nelx + 1) * (nely + 1)))
F = ti.field(ti.f64, shape=(2 * (nelx + 1) * (nely + 1)))
U = ti.field(ti.f64, shape=(2 * (nelx + 1) * (nely + 1)))

# Define boundary conditions
fixed_dofs = list(range(0, 2 * (nely + 1))) # fixed dof
all_dofs = list(range(0, 2 * (nelx + 1) * (nely + 1)))
free_dofs = np.array(list(set(all_dofs) - set(fixed_dofs))) # free dof
n_free_dof = len(free_dofs)

free_dofs_vec = ti.field(ti.i32, shape=n_free_dof)
free_dofs_vec.from_numpy(free_dofs)
K_freedof = ti.field(ti.f64, shape=(n_free_dof, n_free_dof))
F_freedof = ti.field(dtype=ti.f64, shape=n_free_dof)
U_freedof = ti.field(dtype=ti.f64, shape=n_free_dof)


# 配置全局作用力F
F[2 * (nelx + 1) * (nely + 1) - 1] = -1

@ti.kernel
def initialize():
    for i, j in x:
        x[i, j] = volfrac

    for i in range(n_free_dof):
        F_freedof[i] = F[free_dofs_vec[i]]


# Element stiffness matrix
def getK():
    E = 1.0
    nu = 0.3
    k = np.array([1/2-nu/6, 1/8+nu/8, -1/4-nu/12, -1/8+3*nu/8, -1/4+nu/12, -1/8-nu/8, nu/6, 1/8-3*nu/8])
    KE_np = E/(1-nu**2)*np.array([[k[0], k[1], k[2], k[3], k[4], k[5], k[6], k[7]],
                    [k[1], k[0], k[7], k[6], k[5], k[4], k[3], k[2]],
                    [k[2], k[7], k[0], k[5], k[6], k[3], k[4], k[1]],
                    [k[3], k[6], k[5], k[0], k[7], k[2], k[1], k[4]],
                    [k[4], k[5], k[6], k[7], k[0], k[1], k[2], k[3]],
                    [k[5], k[4], k[3], k[2], k[1], k[0], k[7], k[6]],
                    [k[6], k[3], k[4], k[1], k[2], k[7], k[0], k[5]],
                    [k[7], k[2], k[1], k[4], k[3], k[6], k[5], k[0]]])
    KE.from_numpy(KE_np)


# FE analysis
@ti.kernel
def FEAssembleK():
    # 组装全局刚度矩阵
    for I in ti.grouped(K):
        K[I] = 0.

    for ely, elx in ti.ndrange(nely, nelx):
        n1 = (nely + 1) * elx + ely + 1
        n2 = (nely + 1) * (elx + 1) + ely + 1
        edof = ti.Vector([2 * n1 - 2, 2 * n1 - 1, 2 * n2 - 2, 2 * n2 - 1, 2 * n2, 2 * n2 + 1, 2 * n1, 2 * n1 + 1])
        for i, j in ti.static(ti.ndrange(8, 8)):
            K[edof[i], edof[j]] += x[ely, elx] ** penal * KE[i, j]

    # 2. Get K_freedof
    for i, j in ti.ndrange(n_free_dof, n_free_dof):
        K_freedof[i, j] = K[free_dofs_vec[i], free_dofs_vec[j]]


def FESolve():
    # solve
    K_np = K_freedof.to_numpy()
    F_np = F_freedof.to_numpy()
    U_freedof.from_numpy(spsolve(csr_matrix(K_np), F_np))  # scipy solver, will be replaced


@ti.kernel
def FEBackendU():
    # mapping U_freedof backward to U
    for i in range(n_free_dof):
        idx = free_dofs_vec[i]
        U[idx] = U_freedof[i]


@ti.kernel
def get_dc():
    for ely, elx in ti.ndrange(nely, nelx):
        n1 = (nely + 1) * elx + ely + 1
        n2 = (nely + 1) * (elx + 1) + ely + 1
        UE = ti.Vector([U[2 * n1 - 2], U[2 * n1 - 1], U[2 * n2 - 2], U[2 * n2 - 1], U[2 * n2], U[2 * n2 + 1], U[2 * n1], U[2 * n1 + 1]], ti.f64)
        t = ti.Vector([0., 0., 0., 0., 0., 0., 0., 0.], ti.f64)
        for i in ti.static(range(8)):
            for j in ti.static(range(8)):
                t[i] += KE[i, j] * UE[j]
        d = 0.
        for i in ti.static(range(8)):
            d += UE[i] * t[i] # d = Ue' * Ke * Ue

        compliance[None] += x[ely, elx] ** penal * d
        dc[ely, elx] = -penal * x[ely, elx] ** (penal - 1) * d


@ti.kernel
def filt():
    for i, j in ti.ndrange(nelx, nely):
        sum_ = 0.
        dc[j, i] = 0.
        for k in range(ti.max(i - rminf, 0), ti.min(i + rminf + 1, nelx)):
            for l in range(ti.max(j - rminf, 0), ti.min(j + rminf + 1, nely)):
                fac = rmin - ti.sqrt((i - k) ** 2. + (j - l) ** 2.)
                sum_ += ti.max(0., fac)
                dc[j, i] = dc[j, i] + ti.max(0., fac) * x[l, k] * dc_np[l, k]
        dc[j, i] = dc[j, i] / (x[j, i] * sum_)


def oc():
    l1 = 0
    l2 = 100000
    move = 0.2
    x_oc = x.to_numpy()
    x_oc_new = x_oc
    dc_oc = dc.to_numpy()
    while l2 - l1 > 1e-4:
        lmid = 0.5 * (l2 + l1)
        x_oc_new = np.maximum(0.001, np.maximum(x_oc - move, np.minimum(1., np.minimum(x_oc + move, x_oc * np.sqrt(-dc_oc / lmid)))))
        if np.sum(x_oc_new) - volfrac * nelx * nely > 0:
            l1 = lmid
        else:
            l2 = lmid
    x.from_numpy(x_oc_new)


@ti.kernel
def display_sampling():
    s_x = gui_x / nelx
    s_y = gui_y / nely
    for i, j in ti.ndrange(gui_x, gui_y):
        elx = i // s_x
        ely = j // s_y
        display[i, gui_y - j] = 1. - x[ely, elx]  # Note: transpose rho here


initialize()
getK()
loop = 0
change = 1

gui = ti.GUI("99 line topo", res=(gui_x, gui_y))
# video_manager = ti.VideoManager(output_dir='./img', framerate=2, automatic_build=False)
while gui.running:
    while change > 1e-3:
        loop_t = time.time()
        loop += 1
        x_old = x.to_numpy()

        t = time.time()
        FEAssembleK()
        FESolve()
        FEBackendU()
        print("FEM cost time: ", time.time() - t)

        t = time.time()
        get_dc()
        print("cal dc cost time: ", time.time() - t)

        t = time.time()
        dc_np.from_numpy(dc.to_numpy())
        filt()
        print("filt dc cost time: ", time.time() - t)

        t = time.time()
        oc()
        print("oc cost time: ", time.time() - t)

        change = np.max(x_old - x.to_numpy())
        display_sampling()
        # video_manager.write_frame(display)
        print("loop: ", loop, ", volume: ", np.sum(x.to_numpy()) / (nely * nelx), "cost time: ", time.time() - loop_t)
        gui.set_image(display)
        gui.show()
    # video_manager.make_video(gif=True)
    gui.close()